import { BitFlag } from './bfclass';

let FLAGS: string[] = [
    'FL_ONE',
    'FL_TWO',
    'FL_THREE',
    'FL_FOUR'
]

describe('BitFlag class', () => {
    it('must be initialized with a list of up to 64 flags', () => {
        expect(() => { new BitFlag(FLAGS); }).not.toThrow();
        expect(() => { new BitFlag([]); }).toThrow();
        expect(() => { new BitFlag([...Array(66).keys()].map((x:number):string => {
            return `Flag${x}`;
        }))}).toThrow();
    })
    it('must have an internal bit representation of the flags', () => {
        let bf = new BitFlag(FLAGS);
        expect(bf['_getBit']('FL_TWO')).toBe(2);
        expect(bf['_getBit']('FL_THREE')).toBe(4);
        expect(() => { bf['_getBit']('AAA') }).toThrow()
    })
    it('can be initialized with existing states', () => {
        let bf = new BitFlag(FLAGS, 5);
        expect(bf.get('FL_ONE')).toBe(true);
        expect(bf.get('FL_TWO')).toBe(false);
        expect(bf.get('FL_THREE')).toBe(true);
        expect(bf.get('FL_FOUR')).toBe(false);
        expect(bf.as_int()).toBe(5);
    })
    it('can mark a flag as active/true', () => {
        let bf = new BitFlag(FLAGS);
        bf.set('FL_ONE', true);
        expect(bf.get('FL_ONE')).toBe(true);
        expect(bf.as_int()).toBe(1);
    })
    it('can return states as int', () => {
        let bf = new BitFlag(FLAGS, 5);
        expect(bf.as_int()).toBe(5);
    })
    it('can return states as dictionary', () => {
        let bf = new BitFlag(FLAGS, 5);
        let dflag = {
            'FL_ONE': true,
            'FL_TWO': false,
            'FL_THREE': true,
            'FL_FOUR': false,
        }
        expect(bf.as_dict()).toEqual(dflag);
    })
    it('can return states as array', () => {
        let bf = new BitFlag(FLAGS, 5);
        let dflag:[string,boolean][] = [
            ['FL_ONE', true],
            ['FL_TWO', false],
            ['FL_THREE', true],
            ['FL_FOUR', false],
        ]
        expect(bf.as_array()).toEqual(dflag);
    })
    it('can show the differences to another bit states', () => {
        let bf = new BitFlag(FLAGS, 5);
        let dflag = {
            'FL_ONE': false,
            'FL_TWO': true,
        }
        expect(bf.compare(6)).toEqual(dflag);
    })
    it('can update itself to new states', () => {
        let bf = new BitFlag(FLAGS, 5);
        let dflag = {
            'FL_ONE': false,
            'FL_TWO': true,
        }
        expect(bf.update(6)).toEqual(dflag);
        expect(bf.as_int()).toBe(6);
    })
})
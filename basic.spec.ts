import { BIT, MAX } from './basic';
import { newFlag, delFlag, delFlagMulti, addFlag, addFlagMulti } from './basic';
import { check, checkMulti } from './basic';
import { bitcount, prependNum, explodeNum, implodeNum } from './basic'

describe('some basic functions and constants', () => {
    it('has a "BIT" constant for precomputing bit positions into numbers', () => {
        expect(BIT[0]).toBe(1);
        expect(BIT[3]).toBe(8);
        expect(BIT[63]).toBe(1 << 63);
    })
    it('has a "MAX" constant for precomputing max value for a number of bits', () => {
        expect(MAX[0]).toBe(1);
        expect(MAX[1]).toBe(3);
        expect(MAX[2]).toBe(7);
    })
    it('has a "check" function to check if a bit/bits is active (1)', () => {
        expect(check(4,4)).toBe(true);
        expect(check(5,4)).toBe(true);
        expect(check(4,1)).toBe(false);
        expect(check(11,3)).toBe(true);
        expect(check(11,4)).toBe(false);
        expect(check(11,[4,0,4])).toBe(false);
    })
    it('has a "checkMulti" function that check if multiple bit/bits are active (1)', () => {
        expect(checkMulti(11,[1,2])).toBe(true);
        expect(checkMulti(11,[4,0,4])).toBe(false);
        expect(checkMulti(11,[8,2])).toBe(true);
    })
    it('has a "newFlag" function that combine multiple flags(bits) into a single number', () => {
        expect(newFlag([1,2])).toBe(3);
        expect(newFlag([1,2,1])).toBe(3);
        expect(newFlag([2,0])).toBe(2);
        expect(newFlag([1,2,8])).toBe(11);
    })
    it('has a "addFlag" function that apply a bit into an existing number, returning a new number', () => {
        expect(addFlag(0,1)).toBe(1);
        expect(addFlag(8,8)).toBe(8);
        expect(addFlag(8,[2,4,8])).toBe(14);
    })
    it('has a "addFlagMulti" function that apply multiple bits into an existing number, returning a new number', () => {
        expect(addFlagMulti(8,[2,4,8])).toBe(14);
        expect(addFlagMulti(2,[2,0,2])).toBe(2);
    })
    it('has a "delFlag" function that remove a bit/bits from number', () => {
        expect(delFlag(5,1)).toBe(4);
        expect(delFlag(4,1)).toBe(4);
        expect(delFlag(8,1)).toBe(8);
        expect(delFlag(9,1)).toBe(8);
        expect(delFlag(5,2)).toBe(5);
        expect(delFlag(5,4)).toBe(1);
        expect(delFlag(9,[1,2,4])).toBe(8);
    })
    it('has a "delFlagMulti" function that remove a bit/bits from number', () => {
        expect(delFlagMulti(9,[2,4])).toBe(9);
        expect(delFlagMulti(9,[1,2,4])).toBe(8);
        expect(delFlagMulti(11,[1,2,4])).toBe(8);
    })
    it('has a "bitcount" function that can show how many bits required to store a given number', () => {
        expect(bitcount(1)).toBe(1);
        expect(bitcount(4)).toBe(3);
        expect(bitcount(250)).toBe(8);
    })
    it('has a "prepend_num" function that can insert a number at the start (LSB)', () => {
        expect(prependNum(0, 4, 7)).toBe(7);
        expect(prependNum(7, 1, 0)).toBe(14);
        expect(prependNum(7, 2, 1)).toBe(29);
    })
    it('has a "explodeNum" function that can list all number given a list of bit length', () => {
        expect(explodeNum(29, [2,4])).toEqual([1,7]);
        expect(explodeNum(7, [4])).toEqual([7]);
        expect(explodeNum(14, [1,4])).toEqual([0,7]);
    })
    it('has a "implodeNum" function that can convert multiple number into a single number', () => {
        expect(implodeNum([[2,3],[2,3],[2,3],[2,3]])).toBe(255);
        expect(implodeNum([[5,0],[5,0],[5,0]])).toBe(0);
    })
})
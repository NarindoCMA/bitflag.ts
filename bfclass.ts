import { BIT, check, addFlag, delFlag } from './basic';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';

export type BFDict = { [index:string]: boolean }

export class BitFlag {
    protected _int: number;
    protected _flags: string[];

    constructor(flags:string[], states:number=0) {
        if (flags.length == 0) { throw Error('flags have to contain at least 1 flag') }
        if (flags.length > 64) { throw Error('flags have to contain at most 64 flags') }
        this._flags = flags;
        this._int = states;
    }

    get(key:string): boolean {
        return check(this._int, this._getBit(key));
    }

    set(key:string, state:boolean) {
        if (state) {
            this._int = addFlag(this._int, this._getBit(key));
        } else {
            this._int = delFlag(this._int, this._getBit(key));
        }
    }

    as_int(): number {
        return this._int;
    }

    as_dict(): BFDict {
        return [...this._flags].reduce((obj:BFDict, x:string) => {
            obj[x] = check(this._int, this._getBit(x));
            return obj;
        }, {})
    }

    as_array(): [string, boolean][] {
        let result:[string,boolean][] = []
        let bfd = this.as_dict();
        for(let k in bfd) {
          result.push([k, bfd[k]])
        }
        return result;
    }

    compare(newstate:number): BFDict {
        return [...this._flags].reduce((obj:BFDict, x:string) => {
            if (check(this._int, this._getBit(x)) !== check(newstate, this._getBit(x))) {
                obj[x] = check(newstate, this._getBit(x));
            }
            return obj;
        }, {})
    }

    update(newstate:number): BFDict {
        let r = this.compare(newstate);
        this._int = newstate;
        return r;
    }

    protected _getBit(key:string): number {
        let i:number = this._flags.indexOf(key);
        if (i < 0) {
            throw Error('Key not found');
        }
        return BIT[i];
    }
}

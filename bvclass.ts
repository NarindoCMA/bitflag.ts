import { bitcount, explodeNum, implodeNum } from './basic';

export type BVDict = {[index:string]: number}
export type BVMap = Map<string,number>;
type BVVal = {length:number, max:number, value:number}
type BVMapVal = Map<string, BVVal>;

export class KeyNotFoundError extends Error {}

export class BitValue {
    protected _val:BVMapVal = new Map();

    constructor(vals:BVMap, num:number=0) {
        let l:number[] = [];
        let kk:string[] = [];
        let totallength:number = 0;

        if (vals.size == 0) {
            throw Error('vals must have at lease one key:value');
        }
        for (let [k,v] of vals.entries()) {
            let lbc:number = bitcount(v);
            totallength += lbc;
            this._val.set(k, {length:lbc, max:v, value:0})
            if (num > 0) {
                l.push(lbc);
                kk.push(k);
            }
        }
        if (totallength > 64) { throw Error(`max length is 64 bits, got ${totallength}`) }
        if (num > 0) {
            let t = explodeNum(num, l);
            for (let i in kk) {
                this._val.get(kk[i]).value = t[i];
            }
        }
    }

    get(key:string): number {
        if (!this._val.has(key)) { throw new KeyNotFoundError(`${key} not found`) }
        return this._val.get(key).value;
    }

    set(key:string, value:number) {
        if (!this._val.has(key)) { throw new KeyNotFoundError(`${key} not found`) }
        if (value < 0) { throw Error('value must be > 0') }
        if (value > this._val.get(key).max) { throw Error(`max value for ${key} is ${this._val.get(key).max}, got ${value}`) }
        this._val.get(key).value = value;
    }

    as_int(): number {
        return implodeNum([...this._val.values()].map((v:BVVal):[number,number] => {
            return [v.length, v.value];
        }));
    }

    as_dict(): BVDict {
        return [...this._val.entries()].reduce((obj:BVDict, x:[string,BVVal]) => {
            let [k,v] = x;
            obj[k] = v.value;
            return obj;
        }, {});
    }

    as_array(): [string,number][] {
        return [...this._val.entries()].map((x:[string,BVVal]):[string,number] => {
            return [x[0], x[1].value];
        })
    }

    compare(val:number): BVDict {
        let bv = new BitValue(this._getValSpec(), val);
        let td = bv.as_dict();
        return [...this._val.entries()].reduce((obj:BVDict, x:[string, BVVal]) => {
            let [k,v] = x;
            if (this._val.get(k).value != td[k]) { obj[k] = td[k]; }
            return obj;
        }, {});
    }

    update(val:number): BVDict {
        let bv = new BitValue(this._getValSpec(), val);
        let td = bv.as_dict();
        return [...this._val.entries()].reduce((obj:BVDict, x:[string, BVVal]) => {
            let [k,v] = x;
            if (this._val.get(k).value != td[k]) {
                obj[k] = td[k];
                this._val.get(k).value = td[k];
            }
            return obj;
        }, {});
    }

    protected _getValSpec(): BVMap {
        return [...this._val.entries()].reduce((obj:BVMap, x:[string,BVVal]) => {
            let [k,v] = x;
            obj.set(k, v.max);
            return obj;
        }, new Map());
    }
}
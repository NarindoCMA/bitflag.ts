import { BitValue } from './bvclass';

describe('BitValue class', () => {
    const VAL1 = 'Value1'
    const VAL2 = 'Value2'
    const VAL3 = 'Value3'

    let VALUE = new Map([
        ['Value1', 5],
        ['Value2', 100],
        ['Value3', 30],
    ])

    it('must be initialized with a list of numbers 64 bits length total', () => {
        expect(() => { let bv = new BitValue(VALUE) }).not.toThrow();
        expect(() => { let bv = new BitValue(new Map()) }).toThrow();
        let OVER64BIT = new Map([
            ['Value1', 9000000],
            ['Value2', 9000000],
            ['Value3', 9000000],
        ])
        expect(() => { let bv = new BitValue(OVER64BIT) }).toThrow();
    })
    it('has an internal representation of bits, positions, and values', () => {
        let bv = new BitValue(VALUE);
        let tmp = { length:3, max:5, value:0 }
        expect(bv['_val'].get(VAL1)).toEqual(tmp);
        tmp = { length:7, max:100, value:0 }
        expect(bv['_val'].get(VAL2)).toEqual(tmp);
        tmp = { length:5, max:30, value:0 }
        expect(bv['_val'].get(VAL3)).toEqual(tmp);
    })
    it('can be initialized with existing values', () => {
        let bv = new BitValue(VALUE, 31525);
        let tmp = { length:3, max:5, value:5 }
        expect(bv['_val'].get(VAL1)).toEqual(tmp);
        tmp = { length:7, max:100, value:100 }
        expect(bv['_val'].get(VAL2)).toEqual(tmp);
        tmp = { length:5, max:30, value:30 }
        expect(bv['_val'].get(VAL3)).toEqual(tmp);
    })
    it('can get a value, throw error if getting a key that does not exist', () => {
        let bv = new BitValue(VALUE, 31525);
        expect(bv.get(VAL1)).toBe(5);
        expect(() => { bv.get('aa') }).toThrow();
    })
    it('can set a value, throw error if setting a key that does not exist', () => {
        let bv = new BitValue(VALUE);
        expect(()=>{ bv.set(VAL2, 100) }).not.toThrow();
        expect(bv.get(VAL2)).toBe(100);
    })
    it('it will throw an error if setting a key above max value', () => {
        let bv = new BitValue(VALUE);
        expect(()=>{ bv.set(VAL2, 101) }).toThrow();
    })
    it('can return all values as a single number', () => {
        let bv = new BitValue(VALUE);
        bv.set(VAL1, 5);
        bv.set(VAL2, 100);
        bv.set(VAL3, 30);
        expect(bv.as_int()).toBe(31525);
    })
    it('can return all values as a dictionary', () => {
        let bv = new BitValue(VALUE, 31525);
        let vals = {
            'Value1': 5,
            'Value2': 100,
            'Value3': 30,
        }
        expect(bv.as_dict()).toEqual(vals);
    })
    it('can return all values as an array', () => {
        let bv = new BitValue(VALUE, 31525);
        let vals:[string,number][] = [
            ['Value1', 5],
            ['Value2', 100],
            ['Value3', 30],
        ]
        expect(bv.as_array()).toEqual(vals);
    })
    it('can show which value changes', () => {
        let bv = new BitValue(VALUE);
        let vals = {
            'Value1': 5,
        }
        expect(bv.compare(5)).toEqual(vals);
    })
    it('can update to new values', () => {
        let bv = new BitValue(VALUE);
        let vals = {
            'Value1': 5,
        }
        expect(bv.update(5)).toEqual(vals);
        expect(bv.as_int()).toBe(5);
    })
})
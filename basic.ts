export const BIT = [...Array(64).keys()].map((x:number):number => {
    return 1 << x;
});

export const MAX = [...Array(64).keys()].map((i:number):number => {
    return BIT.slice(0,i+1).reduce((x:number,y:number) => { return x|y })
})

export function check(haystack:number, needle:number|number[]): boolean {
    if (typeof(needle) === 'number') {
        return (haystack & needle) === needle;
    } else {
        return checkMulti(haystack, needle);
    }
}

export function checkMulti(haystack:number, needles:number[]): boolean {
    let i = newFlag(needles);
    return (haystack & i) === i;
}

export function newFlag(flags:number[]): number {
    return flags.reduce((x:number, y:number) => { return x|y });
}

export function addFlag(ori:number, flag:number|number[]): number {
    if (typeof(flag) === 'number') {
        return ori | flag;
    } else {
        return addFlagMulti(ori, flag);
    }
}

export function addFlagMulti(ori:number, flags:number[]): number {
    return ori | newFlag(flags);
}

export function delFlag(ori:number, flag:number|number[]): number {
    if (typeof(flag) === 'number') {
        return ori & ~flag;
    } else {
        return delFlagMulti(ori, flag);
    }
}

export function delFlagMulti(ori:number, flags:number[]): number {
    return ori & ~(newFlag(flags));
}

export function bitcount(num:number): number {
    let bit = 0;
    while (num > 0) {
        bit += 1;
        num >>= 1;
    }
    return bit;
}

export function prependNum(initial:number, length:number, value:number): number {
    return (initial << length) | value;
}

export function explodeNum(num:number, lengths:number[]): number[] {
    let result:number[] = [];
    for (length of lengths) {
        result.push(num & MAX[length-1])
        num >>= length;
    }
    return result;
}

export function implodeNum(data:[number,number][]): number {
    let result = 0;
    for (let [length, value] of data.reverse()) {
        result = prependNum(result, length, value);
    }
    return result;
}